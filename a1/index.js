let greeting = 'Hello World';
console.log(greeting);

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName = "Waldrich";
	console.log("First Name: " + firstName);

	let lastName = 'Lee';
	console.log("Last Name: " + lastName)

	let age = 27;
	console.log("My current age is: " + age);

	let gHobbies = "Hobbies: ";
	console.log(gHobbies);

	let hobbies = ["Mixology", "Video Game", "Cycling"];
	console.log(hobbies);
	
	let wAdd = 'Work Address';
	console.log(wAdd);

	let workAddress = {
		houseNumber : 123,
		street : 'Fake Street',
		city : 'Springfield',
		region : 'NCR'
	}
	console.log(workAddress)

let fiend = 'My friends are';
console.log(fiend);

	let friends = ['Sol', 'Hyde', 'Yu', 'Shiki', 'Ragna']
	console.log(friends);

	let profile = {

		username : "Newsleet12" ,
		name: "Waldrich Lee",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let friendName = "Bucky Barnes";
	console.log("My bestfriend is: " + friendName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);


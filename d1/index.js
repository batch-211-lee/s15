console.log("Hello World");

//Javascript renders web pages in an interactive and dynamic fashion. It enables us to create dynamically updating content, control mulitmedia, and animate images.

//Syntax, Statements, and Comments

//Statements in programming are instructions that we tell the computer to perform
//JS Statement usually end with a semicolon ( ; )
//Semicolons are not required but helps us find where a statement ends

//Syntax in programming, it is a set of rules that describes how statements must be constructed

//Comments are parts of the code that gets ignored by the language. They are meant to describe the written code

/*
	There are two types of comments:
	1. Single-line comment (ctrl + /)
	**denoted by two slashes

	2.Multi-line comment ( ctrl + shft + /)
	** denoted byt two slashes and two asteriks in-between


*/

//Variables is used to contain our data. This makes it easier for us to associate information stored in our devices to actual "names" about information

//Declaring Variables - tells our devices that a variable name is created and is ready to store data

let myVariable
console.log(myVariable);

//Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined.

//Syntax
	// let/const variableName;

//console.log() is usefule for printing values of variables or certain results of code into Google Chrome browser's console
//constant use of this throughout developing an application will save us time and builds good habits in always checking for the output of our code

let hello;
console.log(hello); //undefined

//Variables must be declared first before they are used
//using variables before they are declared will return an error

/*
	Guide in writing varibales
		1. use the 'let' keyword followed by our variable name of your choosing and use the assignment operator ( = ) to assign a value
		2. Variable names should start with a lowercase character, use camelCase for multiple words
		3. For constant variables us the 'const' keyword
		4. Variable names should be descriptive or indicative of the value being stored to avoid confusion.
*/

//Declare and initialize variables
	//Initializing variables - is the instance when a variable is given its initial or starting value
	//Syntax
		//let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;
		// const pi = 3.1416 

		//Reassigning variable values- changing its inital or previous value
		//syntax
			//variableName = newValue;
		productName = 'Laptop';
		console.log(productName);

		// console.log(interest);
		// console.log(pi);

		// let variable cannot be re-declared within its scope so this will work:
		// let friend = 'Kate';
		// friend = 'Nej';
		// console.log(friend);

		// let friend = "Kate";
		// let friend = "Nej"; //error
		// console.log(friend);

		// interest = 4.89; //error

//Reassigning variables vs initializing variables
	//Declare variable first

	let supplier;
	supplier = "John Smith Tradings";
	//Initialization is done after the variable have been declared. This is considered as initialization, because it is the first time that a value have been assigned to a variable
	console.log(supplier);

	//considered as reassignment, because its initial value was already declared
	supplier = "Zuitt Store";
	console.log(supplier);

	// const pi;
	// pi = 3.1416; //error, const must be initialized
	// console.log(pi);

	//var
	//var is also used in declaring a variable
	//let/const were introduced as new features in E26 (2015)

	// a = 5;
	// console.log(a); //output : 5
	// var a;

	//Hoisting is JS's default behavior of moving declarations to the top. So in terms of variables and constants, keyword var is hoisted and let and const doesn't allow hoisting

	// a = 5;
	// console.log(a);
	// let a;

//let/const loacl/global scope

	//Scope essentially means where these variables are available for use
	//let and const are block-scoped
	//block is a chunk of code bounded by {}. A block live in a curly braces. Anything within a curly braces is a block.
	//so variable declared in a block with let is only available for use within that block

	let outerVariable = "Hello";
	{
		let innerVariable = "Hello Again";
	}

	console.log(outerVariable);
	// console.log(innerVariable); //innerVariable is not defined

	//Multiple Variable Declaration
	// They may be declared in one line
	//though it is quicker to do without having to retype the "let" keyword, it is still best practice to us multiple "let/const" keywords when declaring variables
	//using multiple keywords makes code easier to determine and read what kind of variable has been created

	// let productCode = 'DC017', productBrand = "Dell";
	let productCode = "DC017";
	let productBrand = 'Dell';
	console.log(productCode, productBrand);


	//using a variable with a reserved keyword
	// const let = "hello";
	// console.log(let); //error

	//Data Type

	//Strings
	//are a series of characters that create a word, phrase, sentence, or anything related to creating text
	//Strings in JS can be written using either a single ('') or double ("") quote.
	//in other programming languages, only the double quotes can be used for creating strings

	let country = 'Philippines';
	let province = "Metro Manila";

	//Concatenating Strings
	//Multiple string values can be combines to create a single string using the "+" symbol

	let fullAddress = province + ', ' + country;
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	//the escape character (\) in strings in combinatin with other character can produce different effects
	// "\n" refers to creating a new line between text

	let mailAddress = 'Metro Manila\n\nPhilippines';
	console.log(mailAddress);
	//Metro Manila
	//
	//Philippines

	//Using the double quotes along with single quotes can allow us to easily include single quote text without using the escape character

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees went home early';
	console.log(message);

	//Numbers

	//Integers/Whole numbers

	let headcount = 26;
	console.log(headcount); //26

	//Decimal Number/Fractions
	let grade = 98.7;
	console.log(grade); //98.7

	//Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance); //20000000000

	//Combining text and strings
	console.log("John's grade last quarter is " + grade);

	//Boolean
	//Boolean values are normally used to store values relating to the states of certain things
	//this will be useful in further discussion about creating logic to make our application respond to certain situations/scenerios

	let isMarried = false;
	let inGoodConduct = true;

	console.log("is Married: " + isMarried);
	console.log("isGoodConduct: " + inGoodConduct);

	//Arrays
	//Arrays are a special kind of data type that is used to store multiple values
	//Arrays can store different data types but is normally used to store similar data types

	//similar data types
	//Syntax
		//let/const arrayName = [elementA, elementB, elementC, ...]
			//Array Literal []
		let grades = [98.7, 75.4, 69.9, 85.3];
		console.log(grades); 

		//different data types
		//storing different data types inside an array is NOT RECOMMENDED, because it will not make sense in the context of the program
		let details = ["john", "Smith", 32, true];
		console.log(details);

		//Onjects
		//Objects are another special kind of data type that is used to mimic real world objects/items
		//they're used to create complex data that contains pieces of information that are relevant to each other
		//Every individual piece of information is called a property of the object

		//Syntax
			//let/const objectName = {
				//propertyA : value,
				// propertyB : value
			//}

		let person = {
			fullName : 'Edward Scissorhands',
			age : 25,
			isMarried : false,
			contact : ["+63723298984", "1234 5678"],
			address : {
				houseNumber : '345',
				city : 'Manila'

			}
		}

		console.log(person);

		let myGrades = {
			firstGrading : 98.7,
			secondGrading : 99,
			thirdGrading : 80 
		}

		console.log(myGrades);

		//type operator

		console.log(typeof myGrades); //object
		console.log(typeof grades); //object
		//note: array is a special type of object with methods and functions to manipulate it
		//we will discuss these methods in later session

		const anime = ['OP', 'OPM', 'AOT', 'BNHA'];

		anime[0] = 'JJK';
		console.log(anime);

		//Null

		//Null simply means a data type was assigned to a variable but it does not hold any value/amount or is nullified
		let spouse = null;
		console.log(spouse);
		//null is considered a data type of its own compared to 0 which is a number and single quotes which are a data type of a string

		let myNumber = 0, myString = '';
		console.log(myNumber);
		console.log(myString);

		//Undefined
		//represents the state of a variable that has been declared but without and assigned value

		let fullName;
		console.log(fullName);